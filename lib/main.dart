import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:log_watcher/screens/device_list_screen.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: _buildTheme(Brightness.dark),
      home: const DeviceListScreen(),
      debugShowCheckedModeBanner: false,
    );
  }

  ThemeData _buildTheme(brightness) {
    var baseTheme = ThemeData(brightness: brightness);

    return baseTheme.copyWith(
      textTheme: GoogleFonts.sourceCodeProTextTheme(baseTheme.textTheme),
    );
  }
}
