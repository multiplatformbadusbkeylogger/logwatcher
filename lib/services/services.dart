import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

import 'package:log_watcher/models/device.dart';
import 'package:log_watcher/models/message.dart';

class APIService {
  Future<List<Device>?> getDevices(BuildContext context) async {
    try {
      const apiUrl = "type_address";

      var url = Uri.http(apiUrl, "/api/log/getDevices");

      // Await the http get response, then decode the json-formatted response.
      var response = await http.get(url, headers: {
        "Access-Control-Allow-Origin": "*",
        'Content-Type': 'application/json',
        'Accept': '*/*'
      });
      if (response.statusCode == 200) {
        var jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;
        return List<Device>.from(jsonResponse.map((x) => Device.fromJson(x)));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<List<Message>?> getMessagesByDeviceMac(
      BuildContext context, String mac) async {
    try {
      const apiUrl = "type_address";
      final queryParameters = {
        'mac': mac,
      };

      var url =
          Uri.http(apiUrl, "/api/log/getMessagesByDevice", queryParameters);

      // Await the http get response, then decode the json-formatted response.
      var response = await http.get(url, headers: {
        "Access-Control-Allow-Origin": "*",
        'Content-Type': 'application/json',
        'Accept': '*/*'
      });
      if (response.statusCode == 200) {
        var jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;
        return List<Message>.from(jsonResponse.map((x) => Message.fromJson(x)));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
