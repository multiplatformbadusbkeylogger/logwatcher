import 'dart:convert';

class Mac {
  final String mac;

  Mac(this.mac);

  Mac.fromJson(Map<String, dynamic> json) : mac = json['mac'] as String;

  Map<String, dynamic> toJson() => {
        'mac': mac,
      };

  String toJsonEncoded() => json.encode(toJson());
}
