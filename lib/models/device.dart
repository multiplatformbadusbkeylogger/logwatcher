class Device {
  Device({this.mac, this.os, this.lastMessage, this.lastDateTime});

  final String? mac;
  final String? os;
  final String? lastMessage;
  final int? lastDateTime;

  factory Device.fromJson(Map<String, dynamic>? json) => Device(
        mac: json?["mac"],
        os: json?["os"],
        lastMessage: json?["lastmessage"],
        lastDateTime: json?["lastdatetime"],
      );

  Map<String, dynamic> toJson() => {
        "mac": mac,
        "os": os,
        "lastmessage": lastMessage,
        "lastdatetime": lastDateTime,
      };
}
