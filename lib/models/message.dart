class Message {
  Message({this.mac, this.os, this.message, this.dateTime});

  final String? mac;
  final String? os;
  final String? message;
  final int? dateTime;

  factory Message.fromJson(Map<String, dynamic>? json) => Message(
        mac: json?["mac"],
        os: json?["os"],
        message: json?["message"],
        dateTime: json?["datetime"],
      );

  Map<String, dynamic> toJson() => {
        "mac": mac,
        "os": os,
        "message": message,
        "datetime": dateTime,
      };
}
