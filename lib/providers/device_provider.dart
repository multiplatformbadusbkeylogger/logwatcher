import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:log_watcher/models/mac.dart';
import 'package:log_watcher/models/message.dart';
import 'package:log_watcher/services/services.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class DeviceProvider extends ChangeNotifier {
  DeviceProvider({required this.mac, required this.title});

  final String mac;
  final String title;
  final String wsUrl = 'ws://type_address/messages';
  List<Message> messageList = [];
  List<Message>? messageHistoryList;
  List<String> emailList = [];
  List<String> phoneNumberList = [];

  WebSocketChannel? channel;

  initWebSocketConnection() async {
    if (this.channel != null) {
      return;
    }
    print("conecting...");
    this.channel = WebSocketChannel.connect(
      Uri.parse(wsUrl),
    );

    print("socket connection initializied");
    final macObject = Mac(mac);
    channel!.sink.add(macObject.toJsonEncoded());
    broadcastNotifications();
  }

  broadcastNotifications() {
    this.channel!.stream.listen((streamData) {
      final messageJson = json.decode(streamData);
      final message = Message.fromJson(messageJson);
      messageList.add(message);
      getSensibleInfo(messageHistoryList, messageList);
      notifyListeners();
    }, onDone: () {
      print("conecting aborted");
      initWebSocketConnection();
    }, onError: (e) {
      print('Server error: $e');
      initWebSocketConnection();
    });
  }

  connectWs() async {
    try {
      return await WebSocket.connect(wsUrl);
    } catch (e) {
      print("Error! can not connect WS connectWs " + e.toString());
      await Future.delayed(Duration(milliseconds: 10000));
      return await connectWs();
    }
  }

  void _onDisconnected() {
    initWebSocketConnection();
  }

  void disconnect() {
    channel!.sink.close();
  }

  Future<List<Message>?> getHistoryMessageList(BuildContext context) async {
    if (messageHistoryList == null || messageHistoryList!.isEmpty) {
      final service = APIService();
      final history = await service.getMessagesByDeviceMac(context, mac);
      messageHistoryList = history;
    }
    return messageHistoryList;
  }

  List<Message>? sortByDate(List<Message> history) {
    history.sort((a, b) => (a.dateTime ?? 0).compareTo(b.dateTime ?? 0));
    return history;
  }

  getSensibleInfo(List<Message>? history, List<Message>? liveList) {
    String messagesConcat = "";
    if (history != null) {
      for (var elem in history) {
        if (elem.message != null) {
          messagesConcat += elem.message!;
        }
      }
    }

    if (liveList != null) {
      for (var elem in liveList) {
        if (elem.message != null) {
          messagesConcat += elem.message!;
        }
      }
    }

    final mailPattern = r"\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b";
    var regEx = RegExp(mailPattern, multiLine: true);
    final obtainedMail =
        regEx.allMatches(messagesConcat.toString()).map((m) => m.group(0));

    emailList.clear();
    var setEmails = HashSet<String>();
    for (var email in obtainedMail) {
      if (email != null) {
        setEmails.add(email);
      }
    }
    emailList = setEmails.toList();
    print(obtainedMail);

    final phoneNumberPattern = r'\b\d{9}\b';
    regEx = RegExp(phoneNumberPattern, multiLine: true);
    final obtainedPhone =
        regEx.allMatches(messagesConcat.toString()).map((m) => m.group(0));

    phoneNumberList.clear();
    var setPhoneNumbers = HashSet<String>();
    for (var phone in obtainedPhone) {
      if (phone != null) {
        setPhoneNumbers.add(phone);
      }
    }
    phoneNumberList = setPhoneNumbers.toList();
  }
}
