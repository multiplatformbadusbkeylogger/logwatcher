import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:log_watcher/models/device.dart';
import 'package:log_watcher/services/services.dart';

class DeviceListProvider extends ChangeNotifier {
  DeviceListProvider();

  Future<List<Device>?> getDeviceList(BuildContext context) async {
    final service = APIService();
    final list = await service.getDevices(context);
    return list;
  }
}
