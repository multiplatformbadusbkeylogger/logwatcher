import 'package:flutter/material.dart';
import 'package:log_watcher/models/message.dart';
import 'package:log_watcher/providers/device_provider.dart';
import 'package:log_watcher/utils/utils.dart';
import 'package:log_watcher/widgets/sync_list_widgets.dart';
import 'package:provider/provider.dart';

final Color screenDividerColor = Colors.red.shade900;

class DeviceScreen extends StatefulWidget {
  const DeviceScreen({
    super.key,
    required this.title,
    required this.mac,
  });

  final String title;
  final String mac;

  @override
  State<DeviceScreen> createState() => _DeviceScreen();
}

class _DeviceScreen extends State<DeviceScreen> {
  @override
  Widget build(BuildContext context) {
    return _DeviceScreenWidget(title: widget.title, mac: widget.mac);
  }
}

class _DeviceScreenWidget extends StatelessWidget {
  const _DeviceScreenWidget({required this.title, required this.mac, Key? key})
      : super(key: key);
  final String title;
  final String mac;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => DeviceProvider(mac: mac, title: title),
        child: const _DeviceScreenDividerWidget());
  }
}

class _DeviceScreenDividerWidget extends StatelessWidget {
  const _DeviceScreenDividerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<DeviceProvider>(context, listen: false);
    provider.initWebSocketConnection();

    return Scaffold(
        appBar: AppBar(title: Text(provider.title)),
        body: WillPopScope(
            onWillPop: () {
              provider.disconnect();
              return Future.value(true);
            },
            child: const Padding(
                padding: EdgeInsets.all(5),
                child: Row(
                  children: [
                    Expanded(
                      child: _MessageHistoryWidget(),
                      flex: 1,
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          Expanded(
                            child: _SocketScreenWidget(),
                            flex: 1,
                          ),
                          Expanded(child: _MessageHighlightsWidget(), flex: 1)
                        ],
                      ),
                      flex: 1,
                    )
                  ],
                ))));
  }
}

class _SocketScreenWidget extends StatelessWidget {
  const _SocketScreenWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<DeviceProvider>(context, listen: true);
    final ScrollController _sc = ScrollController();
    return Container(
        margin: const EdgeInsets.all(2.0),
        padding: const EdgeInsets.all(5.0),
        decoration:
            BoxDecoration(border: Border.all(color: screenDividerColor)),
        child: ListView.builder(
            controller: _sc,
            reverse: false,
            itemCount: provider.messageList.length,
            itemBuilder: (context, index) {
              WidgetsBinding.instance.addPostFrameCallback(
                  (_) => {_sc.jumpTo(_sc.position.maxScrollExtent)});
              return Text(
                  "[${millisToDateString(provider.messageList[index].dateTime)}] ${provider.messageList[index].message ?? ''}",
                  style: TextStyle(color: Color.fromRGBO(0, 255, 102, 1.0)));
            }));
  }
}

class _MessageHistoryWidget extends StatelessWidget {
  const _MessageHistoryWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<DeviceProvider>(context, listen: false);
    return Container(
      margin: const EdgeInsets.all(2.0),
      padding: const EdgeInsets.all(3.0),
      decoration: BoxDecoration(border: Border.all(color: screenDividerColor)),
      child: getFutureList(context, provider),
    );
  }

  Widget getFutureList(BuildContext context, DeviceProvider provider) {
    return FutureBuilder<List<Message>>(
        future: getList(context, provider),
        builder: (context, AsyncSnapshot<List<Message>> snapshot) {
          Widget child;
          if (snapshot.connectionState == ConnectionState.waiting) {
            child = const LoadingIndicator();
          } else {
            if (snapshot.hasError) {
              child = const SyncErrorMessage();
            } else {
              final data = snapshot.data;
              if (data == null || data.isEmpty) {
                child = const NoDataMessage();
              } else {
                child = ListView.builder(
                    itemCount: provider.messageHistoryList!.length,
                    itemBuilder: (context, index) {
                      return Text(
                          "[${millisToDateString(provider.messageHistoryList![index].dateTime)}] ${provider.messageHistoryList![index].message ?? ''}",
                          style: TextStyle(
                              color: Color.fromRGBO(0, 255, 102, 1.0)));
                    });
              }
            }
          }
          return AnimatedSwitcher(
              duration: const Duration(milliseconds: 300), child: child);
        });
  }

  Future<List<Message>> getList(
      BuildContext context, DeviceProvider provider) async {
    final result = await provider.getHistoryMessageList(context);
    if (result == null) {
      return Future.error("");
    }
    return result;
  }
}

class _MessageHighlightsWidget extends StatelessWidget {
  const _MessageHighlightsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<DeviceProvider>(context, listen: true);
    List<String> formatedList = [];
    formatedList.add("     **** DETECTED EMAILS **** ");
    formatedList.addAll(provider.emailList);
    formatedList.add("");
    formatedList.add("     **** DETECTED PHONE NUMBERS ****");
    formatedList.addAll(provider.phoneNumberList);
    return Container(
        margin: const EdgeInsets.all(2.0),
        padding: const EdgeInsets.all(3.0),
        decoration:
            BoxDecoration(border: Border.all(color: screenDividerColor)),
        child: ListView.builder(
            itemCount: formatedList.length,
            itemBuilder: (context, index) {
              return Text(formatedList[index],
                  style: TextStyle(
                      color: Colors.red.shade900, fontWeight: FontWeight.bold));
            }));
  }
}
