import 'package:flutter/material.dart';
import 'package:log_watcher/models/device.dart';
import 'package:log_watcher/providers/device_list_provider.dart';
import 'package:log_watcher/utils/utils.dart';
import 'package:log_watcher/widgets/sync_list_widgets.dart';
import 'package:provider/provider.dart';

import 'screens.dart';

class DeviceListScreen extends StatelessWidget {
  const DeviceListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => DeviceListProvider(), child: const _DeviceListScreen());
  }
}

class _DeviceListScreen extends StatelessWidget {
  const _DeviceListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Devices")),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: _FutureDeviceList(),
      ),
    );
  }
}

class _FutureDeviceList extends StatelessWidget {
  const _FutureDeviceList({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<DeviceListProvider>(context, listen: true);
    return getFutureList(context, provider);
  }

  Widget getFutureList(BuildContext context, DeviceListProvider provider) {
    return FutureBuilder<List<Device>?>(
      future: getList(context, provider),
      builder: (context, AsyncSnapshot<List<Device>?> snapshot) {
        Widget child;
        if (snapshot.connectionState == ConnectionState.waiting) {
          child = const LoadingIndicator();
        } else {
          if (snapshot.hasError) {
            child = const SyncErrorMessage();
          } else {
            final data = snapshot.data;
            if (data == null || data.isEmpty) {
              child = const NoDataMessage();
            } else {
              child = ListView.separated(
                itemCount: data.length,
                itemBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListTile(
                    title: Text(data[index].os!),
                    subtitle: Text(data[index].mac!),
                    trailing:
                        Text(millisToDateString(data[index].lastDateTime!)),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DeviceScreen(
                                  mac: data[index].mac!,
                                  title: "${data[index].os} ${data[index].mac}",
                                )),
                      );
                    },
                  ),
                ),
                separatorBuilder: (_, __) => const Divider(),
              );
            }
          }
        }
        return AnimatedSwitcher(
            duration: const Duration(milliseconds: 300), child: child);
      },
    );
  }

  Future<List<Device>?> getList(
      BuildContext context, DeviceListProvider provider) async {
    final result = await provider.getDeviceList(context);
    if (result == null) {
      return Future.error("");
    }
    return result;
  }
}
