import 'package:intl/intl.dart';

String millisToDateString(int? millis) {
  if (millis == null) {
    return '';
  }
  var dt = DateTime.fromMillisecondsSinceEpoch(millis);
  return DateFormat('dd/MM/yyyy HH:mm:ss').format(dt);
}
